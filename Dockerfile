FROM maven:3-jdk-8 as builder

COPY . /opt/pa-api

WORKDIR /opt/pa-api

RUN mvn dependency:go-offline && mvn package -Dmaven.test.skip=true

FROM openjdk:8-alpine

USER root

COPY --from=builder /opt/pa-api/target/server-jee-0.0.1-SNAPSHOT.jar /usr/src/server-jee-0.0.1-SNAPSHOT.jar

RUN chgrp 0  /usr/src/server-jee-0.0.1-SNAPSHOT.jar && \
    chmod +x  /usr/src/server-jee-0.0.1-SNAPSHOT.jar && \
    chmod g=u /usr/src/server-jee-0.0.1-SNAPSHOT.jar

EXPOSE 8080

USER 1001

CMD ["java", "-jar", "/usr/src/server-jee-0.0.1-SNAPSHOT.jar"]