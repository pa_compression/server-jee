package com.esgi.pa.compression.serverjee.controller;

import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class HomeController {

    private final HttpServletRequest request;

    @Autowired
    public HomeController(HttpServletRequest request) {
        this.request = request;
    }

    @GetMapping(value = "/")
    public String getHome() {
        return "index";
    }

    /*@GetMapping(value = "/logout")
    public String logout() throws ServletException {
        request.logout();
        return "redirect:/";
    }*/

    /*private void configCommonAttributes(Model model) {
        model.addAttribute("name", getKeycloakSecurityContext().getIdToken().getGivenName());
    }*/

    /*@GetMapping(value = "/userinfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserData handleUserInfoRequest(Principal principal) {
        UserData user = new UserData();
        if (principal instanceof KeycloakPrincipal) {

            KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
            AccessToken token = kp.getKeycloakSecurityContext().getToken();
            user.setId(token.getId());
            user.setUserName(token.getName());
            Map<String, Object> otherClaims = token.getOtherClaims();
            user.setCustomAttributes(otherClaims);
        }*/

    private KeycloakSecurityContext getKeycloakSecurityContext() {
        return (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
    }



}
