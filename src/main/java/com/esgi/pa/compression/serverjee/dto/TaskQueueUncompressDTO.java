package com.esgi.pa.compression.serverjee.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TaskQueueUncompressDTO implements TaskQueue {

    public boolean decompression;

    private String folderLocation;

    private String taskId;
}
