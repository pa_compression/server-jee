package com.esgi.pa.compression.serverjee.exception;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException(String id){

        super("Task with id " + id + " not found");
    }
}
