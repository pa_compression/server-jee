package com.esgi.pa.compression.serverjee.model;

public enum TaskType {
    COMPRESS(0), UNCOMPRESS(1);

    private int status;

    private TaskType(int status){
        this.status = status;
    }

    @Override
    public String toString() {
        return "" + this.status;
    }
}
