package com.esgi.pa.compression.serverjee.dto;

import com.esgi.pa.compression.serverjee.model.TaskType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class TaskDTO {

    private String id;

    @NotNull
    private String name;

    @NotNull
    private String mainArgument;

    private int status;

    private LocalDateTime createdAt;

    private TaskType type;
}
