package com.esgi.pa.compression.serverjee;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ServerJeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerJeeApplication.class, args);
    }

    @Bean
    public AmazonSQS getSqsClient(){

        return AmazonSQSClientBuilder.defaultClient();
    }

    @Bean
    public AmazonS3 getS3Client(){

        return AmazonS3ClientBuilder.defaultClient();
    }

    @Bean
    public SendMessageRequest sendMessageRequest(){

        return new SendMessageRequest();
    }
}
