package com.esgi.pa.compression.serverjee.repository;

import com.esgi.pa.compression.serverjee.model.Task;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends PagingAndSortingRepository<Task, String> {

    List<Task> getAllByOwnerIdOrderByUpdatedAtDesc(String ownerId);

    Optional<Task> findByIdAndOwnerId(String id, String ownerId);
}
