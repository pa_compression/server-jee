package com.esgi.pa.compression.serverjee.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Document
@Getter
@Setter
public class Task {

    @Id
    private String id;

    @Field
    private String name;

    @Field
    private String mainArgument;

    @Field
    private int status;

    @Field
    private TaskType type;

    @Field
    @CreatedDate
    private LocalDateTime createdAt;

    @Field
    @LastModifiedDate
    private LocalDateTime updatedAt;

    @Field
    private String ownerId;

    public Task(String name, String mainArgument, String ownerId, TaskType type){

        this.name = name;
        this.mainArgument = mainArgument;
        this.status = 0;
        this.ownerId = ownerId;
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
        this.type = type;
    }


    public String getUrl(){

        return this.mainArgument;
    }

    public void setUrl(String url){

        this.mainArgument = url;
    }

    public String getFolder(){

        return this.mainArgument;
    }

    public void setFolder(String folder){

        this.mainArgument = folder;
    }
}

