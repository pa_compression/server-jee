package com.esgi.pa.compression.serverjee.controller;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.esgi.pa.compression.serverjee.dto.FileDTO;
import com.esgi.pa.compression.serverjee.dto.TaskDTO;
import com.esgi.pa.compression.serverjee.exception.FileNotFoundException;
import com.esgi.pa.compression.serverjee.model.File;
import com.esgi.pa.compression.serverjee.model.Task;
import com.esgi.pa.compression.serverjee.repository.InvalidFieldException;
import com.esgi.pa.compression.serverjee.services.FileService;
import com.esgi.pa.compression.serverjee.services.QueueService;
import com.esgi.pa.compression.serverjee.services.TaskService;
import lombok.RequiredArgsConstructor;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/file")
@RequiredArgsConstructor
public class FileController {

    private final FileService fileService;
    private final TaskService taskService;
    private final QueueService queueService;
    private final AmazonS3 amazonS3;

    @Value("${api.aws.resultBucket}")
    private String fileResultBucket;

    @GetMapping
    public List<FileDTO> getAllFiles(KeycloakAuthenticationToken token) throws Exception {

        List<File> fileList = fileService.getAllFiles(this.getKeycloakSubject(token));

        return fileList.stream().map(this::fromFileToFileDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public FileDTO getFileById (@PathVariable("id") String id, KeycloakAuthenticationToken token) {

        File file = fileService.getOne(id, this.getKeycloakSubject(token)).orElseThrow(() -> new FileNotFoundException(id));

        return this.fromFileToFileDTO(file);
    }

    @PostMapping("/{id}")
//    @PreAuthorize("hasRole('CREATE_FILE')")
    // Only call by compression app
    public ResponseEntity<?> addFile(@RequestBody @Valid FileDTO file, @PathVariable("id") String taskId, HttpServletRequest request) {

        try{

            File newFile = fileService.create(file, taskId);

            return ResponseEntity.created(URI.create(request.getLocalAddr() + "/file/" + newFile.getId())).build();

        }catch (InvalidFieldException e){

            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/finishUncompressTask/{id}")
    public ResponseEntity<?> finishUncompressTask(@PathVariable("id") String taskId){

        try{

            Task findTask = fileService.finishUncompressTask(taskId);

            return ResponseEntity.ok().build();

        }catch (InvalidFieldException e){

            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/download/{id}/compress")
    public void downloadCompress(@PathVariable("id") String id, HttpServletResponse response) throws IOException {

        File file = fileService.getForDownload(id).orElseThrow(() -> new FileNotFoundException(id));

        response.sendRedirect(this.downloadS3Artifact(file, "_compressed").toString());
    }

    @GetMapping("/download/{id}/uncompress")
    public void downloadUncompress(@PathVariable("id") String id, HttpServletResponse response) throws IOException {

        File file = fileService.getForDownload(id).orElseThrow(() -> new FileNotFoundException(id));

        response.sendRedirect(this.downloadS3Artifact(file, "_compressed_decompressed").toString());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id, KeycloakAuthenticationToken token){

        try{

            this.fileService.deleteFileAndTask(id, this.getKeycloakSubject(token));

            return ResponseEntity.ok().build();

        }catch (InvalidFieldException e){

            return ResponseEntity.notFound().build();
        }
    }

    private URL downloadS3Artifact(File file, String prefix){

        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
                fileResultBucket,
                file.getFolderUuid()
                        + prefix + ".tar.gz"
        ).withExpiration(getExpirationTimeDownloadLink());

        return amazonS3.generatePresignedUrl(generatePresignedUrlRequest);
    }

    @GetMapping("/{id}/uncompress")
    public TaskDTO uncompress(@PathVariable("id") String fileId, KeycloakAuthenticationToken token){

        File requestFile = this.fileService.getOne(fileId, this.getKeycloakSubject(token)).orElseThrow(() -> new FileNotFoundException(fileId));
        Task uncompressTask = this.taskService.createUncompressTask(requestFile, this.getKeycloakSubject(token));
        TaskDTO taskDTO = TaskDTO.builder()
                .id(uncompressTask.getId())
                .name(uncompressTask.getName())
                .mainArgument(uncompressTask.getFolder())
                .status(uncompressTask.getStatus())
                .createdAt(uncompressTask.getCreatedAt())
                .type(uncompressTask.getType())
            .build();

        try{

            queueService.sendUncompressTask(taskDTO);
            fileService.setUncompressTask(requestFile, uncompressTask);

        }catch (InvalidFieldException e){

            taskService.updateStatus(uncompressTask.getId(), 3);
        }

        return taskDTO;
    }



    private Date getExpirationTimeDownloadLink(){

        Date expiration = new Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 300000;
        expiration.setTime(expTimeMillis);

        return expiration;
    }

    private String getKeycloakSubject(KeycloakAuthenticationToken token){

        KeycloakSecurityContext context = (KeycloakSecurityContext) token.getCredentials();

        return context.getToken().getSubject();
    }

    private FileDTO fromFileToFileDTO(File file){

        return FileDTO.builder()
                .id(file.getId())
                .name(file.getName())
                .folderUuid(file.getFolderUuid())
                .taskId(file.getTaskId())
                .uncompressTaskId(file.getUncompressTaskId())
                .uncompressTaskAvailable(file.isUncompressTaskAvailable())
                .createdAt(file.getCreatedAt())
                .build();
    }
}
