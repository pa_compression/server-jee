package com.esgi.pa.compression.serverjee.exception;

public class FileNotFoundException extends RuntimeException {

    public FileNotFoundException(String id){

        super("File with id " + id + " not found");
    }
}
