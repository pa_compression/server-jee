package com.esgi.pa.compression.serverjee.repository;

import com.esgi.pa.compression.serverjee.model.File;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FileRepository extends CrudRepository<File, String> {

    List<File> findAllByUserIdOrderByUpdatedAtDesc(String ownerId);

    Optional<File> findByIdAndUserId(String id, String ownerId);

    void deleteByIdAndUserId(String id, String ownerId);

    Optional<File> findByUncompressTaskId(String taskId);
}
