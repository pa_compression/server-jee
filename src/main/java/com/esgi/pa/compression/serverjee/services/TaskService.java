package com.esgi.pa.compression.serverjee.services;

import com.esgi.pa.compression.serverjee.dto.TaskDTO;
import com.esgi.pa.compression.serverjee.model.File;
import com.esgi.pa.compression.serverjee.model.Task;
import com.esgi.pa.compression.serverjee.model.TaskType;
import com.esgi.pa.compression.serverjee.repository.TaskRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    public List<Task> getAll(String ownerId){

        return taskRepository.getAllByOwnerIdOrderByUpdatedAtDesc(ownerId);
    }

    public Optional<Task> getOne(String id, String ownerId){

        return taskRepository.findByIdAndOwnerId(id, ownerId);
    }

    public Task createCompressTask(TaskDTO toCreate, String ownerId){

        Task newTask = new Task(toCreate.getName(), toCreate.getMainArgument(), ownerId, TaskType.COMPRESS);

        return taskRepository.save(newTask);
    }

    public Task createUncompressTask(File file, String ownerId){

        Task newTask = new Task(file.getName() + " uncompress", file.getFolderUuid(), ownerId, TaskType.UNCOMPRESS);

        return taskRepository.save(newTask);
    }

    public Optional<Task> update(String id, TaskDTO data, String ownerId){

        Optional<Task> find = taskRepository.findByIdAndOwnerId(id, ownerId);

        return find.map(task -> {
            task.setUrl(data.getMainArgument() != null ? data.getMainArgument() : task.getUrl());
            task.setName(data.getName() != null ? data.getName() : task.getName());
            task.setUpdatedAt(LocalDateTime.now());

            return taskRepository.save(task);
        });
    }

    public Optional<Task> updateStatus(String id, int status){

        Optional<Task> find = taskRepository.findById(id);

        return find.map(task -> {
            task.setStatus(status);
            task.setUpdatedAt(LocalDateTime.now());
           return taskRepository.save(task);
        });
    }
}
