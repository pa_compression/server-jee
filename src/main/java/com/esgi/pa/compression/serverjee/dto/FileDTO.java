package com.esgi.pa.compression.serverjee.dto;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@Getter
public class FileDTO {

    private String id;

    private String name;

    @NotNull
    private String folderUuid;

    private LocalDateTime createdAt;

    private String taskId;

    private String uncompressTaskId;

    private boolean uncompressTaskAvailable;
}
