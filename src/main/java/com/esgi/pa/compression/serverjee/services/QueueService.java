package com.esgi.pa.compression.serverjee.services;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.esgi.pa.compression.serverjee.dto.TaskDTO;
import com.esgi.pa.compression.serverjee.dto.TaskQueue;
import com.esgi.pa.compression.serverjee.dto.TaskQueueCompressDTO;
import com.esgi.pa.compression.serverjee.dto.TaskQueueUncompressDTO;
import com.esgi.pa.compression.serverjee.model.TaskType;
import com.esgi.pa.compression.serverjee.repository.InvalidFieldException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class QueueService {

    private final ObjectMapper objectMapper;
    private final AmazonSQS sqsQueue;
    private final SendMessageRequest messageRequest;

    @Value("${api.aws.sqsQueue}")
    private String sqsQueueUrl;

    public String sendCompressionTask(TaskDTO taskDTO) throws InvalidFieldException {

        if(taskDTO.getType() != TaskType.COMPRESS)

            throw new InvalidFieldException();

        TaskQueueCompressDTO taskQueueCompressDTO = TaskQueueCompressDTO.builder()
                .compression(true)
                .gitUrl(taskDTO.getMainArgument())
                .taskId(taskDTO.getId())
                .build();

        return this.sendToQueue(taskQueueCompressDTO);
    }

    public String sendUncompressTask(TaskDTO taskDTO) throws InvalidFieldException {

        if(taskDTO.getType() != TaskType.UNCOMPRESS)

            throw new InvalidFieldException();

        TaskQueueUncompressDTO taskQueueCompressDTO = TaskQueueUncompressDTO.builder()
                .decompression(true)
                .folderLocation(taskDTO.getMainArgument() + "_compressed")
                .taskId(taskDTO.getId())
                .build();

        return this.sendToQueue(taskQueueCompressDTO);
    }

    private String sendToQueue(TaskQueue taskQueue) throws InvalidFieldException {
        try{

            String queueBody = objectMapper.writeValueAsString(taskQueue);

            return sqsQueue.sendMessage(messageRequest.withQueueUrl(sqsQueueUrl)
                    .withMessageBody(queueBody)).getMessageId();

        }catch (Exception e){

            throw new InvalidFieldException();
        }
    }
}
