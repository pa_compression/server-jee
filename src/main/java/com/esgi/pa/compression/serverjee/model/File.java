package com.esgi.pa.compression.serverjee.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Document
@Getter
@Setter
public class File {

    @Id
    private String id;
    @Field
    private String name;
    @Field
    private String folderUuid;
    @Field
    @CreatedDate
    private LocalDateTime createdAt;
    @Field
    @LastModifiedDate
    private LocalDateTime updatedAt;
    @Field
    private String taskId;
    @Field
    private String uncompressTaskId;
    @Field
    private boolean uncompressTaskAvailable;
    @Field
    private String userId;

    public File(String name, String folderUuid, String taskId, String userId) {
        this.name = name;
        this.folderUuid = folderUuid;
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
        this.taskId = taskId;
        this.userId = userId;
        this.uncompressTaskAvailable = false;
    }
}

