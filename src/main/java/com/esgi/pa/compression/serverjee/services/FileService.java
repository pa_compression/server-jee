package com.esgi.pa.compression.serverjee.services;

import com.esgi.pa.compression.serverjee.dto.FileDTO;
import com.esgi.pa.compression.serverjee.model.File;
import com.esgi.pa.compression.serverjee.model.Task;
import com.esgi.pa.compression.serverjee.model.TaskType;
import com.esgi.pa.compression.serverjee.repository.FileRepository;
import com.esgi.pa.compression.serverjee.repository.InvalidFieldException;
import com.esgi.pa.compression.serverjee.repository.TaskRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class FileService {

    private final FileRepository fileRepository;
    private final TaskRepository taskRepository;

    public List<File> getAllFiles (String ownerId)  {

        return fileRepository.findAllByUserIdOrderByUpdatedAtDesc(ownerId);
    }

    public Optional<File> getOne(String id, String ownerId){

        return fileRepository.findByIdAndUserId(id, ownerId);
    }

    public Optional<File> getForDownload(String id){

        return fileRepository.findById(id);
    }

    public File create(FileDTO toCreate, String taskId){

        Task findTask = taskRepository.findById(taskId).orElseThrow(InvalidFieldException::new);

        File newFile = new File(findTask.getName(), toCreate.getFolderUuid(), findTask.getId(), findTask.getOwnerId());

        findTask.setStatus(2);
        findTask.setUpdatedAt(LocalDateTime.now());
        taskRepository.save(findTask);

        return fileRepository.save(newFile);
    }

    public File setUncompressTask(File file, Task uncompressTask){

        if(uncompressTask.getType() != TaskType.UNCOMPRESS)

            throw new InvalidFieldException();

        file.setUncompressTaskId(uncompressTask.getId());
        file.setUncompressTaskAvailable(false);
        file.setUpdatedAt(LocalDateTime.now());
        fileRepository.save(file);

        return file;
    }

    public Task finishUncompressTask(String taskId){

        Task task = taskRepository.findById(taskId).orElseThrow(InvalidFieldException::new);
        File file = fileRepository.findByUncompressTaskId(taskId).orElseThrow(InvalidFieldException::new);

        task.setStatus(2);
        task.setUpdatedAt(LocalDateTime.now());
        taskRepository.save(task);

        file.setUncompressTaskAvailable(true);
        file.setUpdatedAt(LocalDateTime.now());
        fileRepository.save(file);

        return task;
    }

    public void deleteFileAndTask(String id, String userId) throws InvalidFieldException{

        File file = fileRepository.findByIdAndUserId(id, userId).orElseThrow(InvalidFieldException::new);

        if(file.getTaskId() != null)

            taskRepository.deleteById(file.getTaskId());

        if(file.getUncompressTaskId() != null)

            taskRepository.deleteById(file.getUncompressTaskId());

        fileRepository.deleteByIdAndUserId(id, userId);
    }
}
