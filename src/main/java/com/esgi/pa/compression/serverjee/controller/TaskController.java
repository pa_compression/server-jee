package com.esgi.pa.compression.serverjee.controller;

import com.esgi.pa.compression.serverjee.dto.TaskDTO;
import com.esgi.pa.compression.serverjee.exception.TaskNotFoundException;
import com.esgi.pa.compression.serverjee.model.Task;
import com.esgi.pa.compression.serverjee.repository.InvalidFieldException;
import com.esgi.pa.compression.serverjee.services.QueueService;
import com.esgi.pa.compression.serverjee.services.TaskService;
import lombok.RequiredArgsConstructor;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;
    private final QueueService queueService;


    @GetMapping
    public List<TaskDTO> getAllCompressTasks(KeycloakAuthenticationToken token) {
        List<Task> fileList = taskService.getAll(this.getKeycloakSubject(token));

        return fileList.stream().map(this::fromTaskToTaskDTO)
                                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public TaskDTO getTaskById(@PathVariable("id") String id, KeycloakAuthenticationToken token) {

        Task task = taskService.getOne(id, this.getKeycloakSubject(token)).orElseThrow(() -> new TaskNotFoundException(id));

        return this.fromTaskToTaskDTO(task);
    }

    @PostMapping
    public ResponseEntity<TaskDTO> addTask(@RequestBody @Valid TaskDTO task, HttpServletRequest request, KeycloakAuthenticationToken token) {

        Task newTask = taskService.createCompressTask(task, this.getKeycloakSubject(token));
        TaskDTO taskDTO = this.fromTaskToTaskDTO(newTask);

        try{

            queueService.sendCompressionTask(taskDTO);

        }catch (InvalidFieldException e){

            System.out.println("Error envoie queue");

            taskService.updateStatus(newTask.getId(), 3);
        }

        return ResponseEntity.created(URI.create(request.getLocalAddr() + "/task/" + newTask.getId()))
                .body(taskDTO);
    }

    @PutMapping("/{id}")
    public TaskDTO updateTask(@PathVariable("id") String id, @RequestBody TaskDTO taskDTO, KeycloakAuthenticationToken token) {

        Task updatedTask  = taskService.update(id, taskDTO, this.getKeycloakSubject(token)).orElseThrow(() -> new TaskNotFoundException(id));

        return this.fromTaskToTaskDTO(updatedTask);
    }

    @PutMapping("/{id}/status/{status}")
    public TaskDTO updateStatus(@PathVariable("id") String id, @PathVariable("status") int status) {

        Task updatedTask  = taskService.updateStatus(id, status).orElseThrow(() -> new TaskNotFoundException(id));

        return this.fromTaskToTaskDTO(updatedTask);
    }

    private String getKeycloakSubject(KeycloakAuthenticationToken token){

        KeycloakSecurityContext context = (KeycloakSecurityContext) token.getCredentials();

        return context.getToken().getSubject();
    }

    private TaskDTO fromTaskToTaskDTO(Task task){

        return TaskDTO.builder()
                    .id(task.getId())
                    .name(task.getName())
                    .mainArgument(task.getUrl())
                    .status(task.getStatus())
                    .createdAt(task.getCreatedAt())
                    .type(task.getType())
                .build();
    }
}
