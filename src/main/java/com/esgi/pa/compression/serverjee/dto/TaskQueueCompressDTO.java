package com.esgi.pa.compression.serverjee.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TaskQueueCompressDTO implements TaskQueue {

    public boolean compression;

    private String gitUrl;

    private String taskId;
}
